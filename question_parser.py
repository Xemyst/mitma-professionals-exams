from enums.question_type import QuestionType
from question import Question
import re
class QuestionParser:
    question_dic_format= {"code": "COD: ", "question": "PREGUNTA: ", "a": "A: ", "b": "B: ", "c": "C: ", "d" : "D: ", "solution" : "SOLUCION: ", "rule": "NORMA: "}

    def parse_questions(self, raw_questions: str) -> dict :
        questions_information = {"question_type": "", "questions": []}
        question_type = self._identify_question_type(raw_questions);
        if(question_type == QuestionType.COMPETENCIA_PROFESIONAL_PREGUNTAS):
            questions = self._parse_competencia_professional_questions(raw_questions, question_type)
        elif(question_type == QuestionType.COMPETENCIA_PROFESIONAL_PRACTICOS):
            questions = self._parse_competencia_professional_practical_questions(raw_questions, question_type)
        elif(question_type == QuestionType.CONDUCTORES_PROFESSIONALES):
            questions = self._parse_conductor_professional_questions(raw_questions, question_type)
        elif(question_type == QuestionType.CONSEJERO_SEGURIDAD_PREGUNTAS):
            questions = self._parse_consejero_seguridad_questions(raw_questions, question_type)
        elif(question_type == QuestionType.CONSEJERO_SEGURIDAD_PRACTICOS):
            questions = self._parse_consejero_seguridad_practical_questions(raw_questions, question_type)
        questions_information["question_type"] = question_type
        questions_information["questions"] = questions

        return questions_information
    
        

    def _identify_question_type(self, raw_questions: str):
        if (bool(re.search(
            r'(COD:.*\nPREGUNTA:.+\nA:.+\nB:.+\nC:.+\nD:.+\nSOLUCION:.*\nNORMA:.*)', 
            raw_questions))):
            return QuestionType.COMPETENCIA_PROFESIONAL_PREGUNTAS
        elif(bool(re.search(
            r'(RESPUESTA A:[\d\s\S]+RESPUESTA B:[\d\s\S]+RESPUESTA C:[\d\s\S]+RESPUESTA D:[\d\s\S]+RESPUESTA E:[\d\s\S]+RESPUESTA F:[\d\s\S]+SOLUCION:.+\nNORMA:.+)',
            raw_questions
        ))):
            return QuestionType.COMPETENCIA_PROFESIONAL_PRACTICOS
        elif(bool(re.search(
            r'(A.+\n\nB.+\n\nC.+\n\nD.+\n\nRESPUESTA:.+\n\nNORMA:.+\n\n(Referencia doctrinal|REFERENCIA DOCTRINAL):)',
            raw_questions
        ))):
            return QuestionType.CONDUCTORES_PROFESSIONALES
        elif(bool(re.search(
            r'(Identificador:.*\nEnunciado:\n.+\n1.-.+\n2.-.+\n3.-.+\n4.-.+\nRespuesta:.*\nNorma:.*)', 
            raw_questions))):
            return QuestionType.CONSEJERO_SEGURIDAD_PREGUNTAS
        else:
            return QuestionType.CONSEJERO_SEGURIDAD_PRACTICOS 
    
    def _parse_consejero_seguridad_questions(self, raw_questions, question_type):
        questions = []
        raw_questions = raw_questions.split('\n\n')
        for question in raw_questions:
            if question != '' :
                questions += [self._parse_consejero_seguridad_question(question, question_type)]

        return questions

    def _parse_consejero_seguridad_question(self, raw_question, question_type):
        try:
            code_pattern = re.compile(r"(?<=Identificador: ).+")
            code = re.findall(code_pattern, raw_question)[0]
            question_pattern = re.compile(r"(?<=Enunciado:\n).+")
            question = re.findall(question_pattern, raw_question)[0]
            a_pattern = re.compile(r"(?<=1.- ).+")
            a = re.findall(a_pattern, raw_question)[0]
            b_pattern = re.compile(r"(?<=2.- ).+")
            b = re.findall(b_pattern, raw_question)[0]
            c_pattern = re.compile(r"(?<=3.- ).+")
            c = re.findall(c_pattern, raw_question)[0]
            d_pattern = re.compile(r"(?<=4.- ).+")
            d = re.findall(d_pattern, raw_question)[0]
            solution_pattern = re.compile(r"(?<=Respuesta: ).+")
            solution = re.findall(solution_pattern, raw_question)[0]
            rule_pattern = re.compile(r"(?<=Norma: ).+")
            rule = re.findall(rule_pattern, raw_question)[0]
        except:
            print(raw_question)
            raise

        dic = {
            "code": code,
            "question": question,
            "1": a,
            "2": b,
            "3": c,
            "4": d,
            "solution": solution,
            "rule": rule,
        }

        question = Question(dic, question_type)
        return question

    def _parse_consejero_seguridad_practical_questions(self, raw_questions, question_type):
        out_questions = []
        question_pattern = re.compile(r'Identificado')
        questions = re.split(question_pattern, raw_questions)[1:]
        for question in questions:
            if question != '' :
                out_questions += [self._parse_consejero_seguridad_practical_question(question, question_type)]
        return out_questions

    def _parse_consejero_seguridad_practical_question(self, raw_question: str, question_type) -> Question:
        try: 
            code_pattern = re.compile(r"(?<=r: ).+")
            code = re.findall(code_pattern, raw_question)[0]

            question_pattern = re.compile(r"(?<=Enunciado:\n).+")
            question = re.findall(question_pattern, raw_question)[0]

            solution_pattern = re.compile(r"(?<=Respuesta: )[\d\S\s]+(?=Normativa:)")
            solution = re.findall(solution_pattern, raw_question)[0]
            
            rule_pattern = re.compile(r"(?<=Normativa:)[\d\S\s]+")
            rule = re.findall(rule_pattern, raw_question)[0]

            dic = {
                "code": code,
                "question": question,
                "solution": solution,
                "rule": rule,
            }
            question = Question(dic, question_type)
        except: 
            print(raw_question)
            raise
        return question

    def _parse_conductor_professional_questions(self, raw_questions, question_type: QuestionType):
        questions = []
        raw_questions = raw_questions.split('\n\n\n\n')
        for question in raw_questions:
            if question != '' :
                questions += [self._parse_conductor_professional_question(question, question_type)]

        return questions

    def _parse_conductor_professional_question(self, raw_question: str, question_type: QuestionType) -> Question:
        code_pattern = re.compile(r"(?<=COD: ).+")
        code = re.findall(code_pattern, raw_question)[0]
        question = raw_question.split('\n\n')[1]
        a_pattern = re.compile(r"(?<=\nA ).+")
        a = re.findall(a_pattern, raw_question)[0]
        b_pattern = re.compile(r"(?<=B ).+")
        b = re.findall(b_pattern, raw_question)[0]
        c_pattern = re.compile(r"(?<=C ).+")
        c = re.findall(c_pattern, raw_question)[0]
        d_pattern = re.compile(r"(?<=D ).+")
        d = re.findall(d_pattern, raw_question)[0]
        solution_pattern = re.compile(r"(?<=RESPUESTA: ).+")
        solution = re.findall(solution_pattern, raw_question)[0]
        rule_pattern = re.compile(r"(?<=NORMA: ).+")
        rule = re.findall(rule_pattern, raw_question)[0]

        ref_pattern = re.compile(r"(?<=Referencia doctrinal: ).*")
        try:
            ref = re.findall(ref_pattern, raw_question)[0]
        except:
            ref_pattern = re.compile(r"(?<=REFERENCIA DOCTRINAL: ).*")
            ref = re.findall(ref_pattern, raw_question)[0]
        dic = {
            "code": code,
            "question": question,
            "a": a,
            "b": b,
            "c": c,
            "d": d,
            "solution": solution,
            "rule": rule,
            "doctrinal_reference": ref
        }

        question = Question(dic, question_type)
        return question

    def _parse_competencia_professional_questions(self, raw_questions, question_type: QuestionType):
        raw_questions = raw_questions.split('\n\n')
        questions = []
        for question in raw_questions:
            if question != '' :
                questions += [self._parse_competencia_professional_question(question, question_type)]
        return questions


    def _parse_competencia_professional_practical_questions(self, raw_questions, question_type):
        output_questions = []
        code_pattern = re.compile(r'(?<=COD: ).+')
        codes = re.findall(code_pattern, raw_questions)
        question_pattern = re.compile(r'(?<=COD: ).+')
        questions = re.split(question_pattern, raw_questions)[1:]
        for id, question in enumerate(questions):
            if question != '' :
                output_questions += [self._parse_competencia_professional_practical_question(codes[id], question, question_type)]

        return output_questions

    def _parse_competencia_professional_practical_question(self, code, raw_question, question_type: QuestionType):
        try:
            question_pattern = re.compile(r"(?<=PREGUNTA:\n)([\d\s\S]+)(?=RESPUESTA A:)")
            question = re.findall(question_pattern, raw_question)[0]
            a_pattern = re.compile(r"(?<=RESPUESTA A:\n)([\d\s\S]+)(?=RESPUESTA B:)")
            a = re.findall(a_pattern, raw_question)[0]
            b_pattern = re.compile(r"(?<=RESPUESTA B:\n)([\d\s\S]+)(?=RESPUESTA C:)")
            b = re.findall(b_pattern, raw_question)[0]
            c_pattern = re.compile(r"(?<=RESPUESTA C:\n)([\d\s\S]+)(?=RESPUESTA D:)")
            c = re.findall(c_pattern, raw_question)[0]
            d_pattern = re.compile(r"(?<=RESPUESTA D:\n)([\d\s\S]+)(?=RESPUESTA E:)")
            d = re.findall(d_pattern, raw_question)[0]
            f_pattern = re.compile(r"(?<=RESPUESTA E:\n)([\d\s\S]+)(?=RESPUESTA F:)")
            f = re.findall(f_pattern, raw_question)[0]
            e_pattern = re.compile(r"(?<=RESPUESTA F:\n)([\d\s\S]+)(?=RESPUESTA G:)")
            e = re.findall(e_pattern, raw_question)[0]
            g_pattern = re.compile(r"(?<=RESPUESTA G:\n)([\d\s\S]+)(?=RESPUESTA H:)")
            g = re.findall(g_pattern, raw_question)[0]
            h_pattern = re.compile(r"(?<=RESPUESTA H:\n)([\d\s\S]+)(?=SOLUCION:)")
            h = re.findall(h_pattern, raw_question)[0]
            solution_pattern = re.compile(r"(?<=SOLUCION: ).+")
            solution = re.findall(solution_pattern, raw_question)[0]
            rule_pattern = re.compile(r"(?<=NORMA: ).*")
            rule = re.findall(rule_pattern, raw_question)[0]
        except:
            print(raw_question)
            raise

        dic =  {
            "code": code,
            "question": question,
            "a": a,
            "b": b,
            "c": c,
            "d": d,
            "e": e,
            "f": f,
            "g": g,
            "h": h,
            "solution": solution,
            "rule": rule 
        }

        question = Question(dic, question_type)
        return question

    def _parse_competencia_professional_question(self, question, question_type: QuestionType):
        dic = {}
        for line in question.split("\n"):
            header = self._get_header(line)
            if(header == None): continue
            dic[header] = line.split(self.question_dic_format[header])[1]
        self.code = dic["code"]
        self.question = dic["question"]
        self.a = dic["a"]
        self.b = dic["b"]
        self.c = dic["c"]
        self.d = dic["d"]
        self.answer = dic["solution"]
        self.law = dic["rule"]
        question = Question(dic, question_type)
        return question

    def _get_header(self, line):
        if(line.startswith("COD: ")): return "code"
        elif(line.startswith("PREGUNTA: ")): return "question"
        elif(line.startswith("A")): return "a"
        elif(line.startswith("B")): return "b"
        elif(line.startswith("C")): return "c"
        elif(line.startswith("D")): return "d"
        elif(line.startswith("SOLUCION: ")): return "solution"
        elif(line.startswith("NORMA: ")): return "rule"