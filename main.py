from question_parser import QuestionParser
import json
import os
from exam import Exam

from pylatex import Document, Section, Subsection, Command, LineBreak
from pylatex.utils import italic, NoEscape, bold
from pylatex.base_classes.command import Options

def _write_output(path: str, data: str, encoding='utf-8'): 
    file = open(path, 'w', encoding=encoding)
    file.write(data)
    file.close()

def _read_file(path, encoding = 'utf-8'): 
    file = open(path, 'r', encoding=encoding)
    return file.read()


def parse_questions(input_data):
    qp = QuestionParser()
    return qp.parse_questions(input_data);

def add_questions(doc, questions):
    for question in questions:
        add_question(doc, question)

def _get_keys_of_options():
    return ["A","B","C","D"]

def _add_key_value(doc, key,value):
    doc.append(bold(key))
    doc.append(value)
    doc.append("\n")

def _add_value(doc, value):
    doc.append(value)
    doc.append("\n")

def add_question(doc, question):
    _add_key_value(doc,"CODE: ", question["COD"] )
    _add_value(doc, question["PREGUNTA"])

    for option in _get_keys_of_options():
        _add_key_value(doc, f'${option} ', question[option])

    _add_key_value(doc,"Solució: ", question["SOLUCION"] )
    # _add_key_value(doc,"Norma: ", question["NORMA"] )
    doc.append("\n")

    

def parse_competencia_professional_mercancias_questions(doc):
    title = 'Competencia professional'
    with doc.create(Section(title)):
        date = '04-03-2022'
        letters = ['A','B','C','D','E','F','G','H']
        for letter in letters:
            file_name = f'pm1{letter}'
            input_path = f'data/{date}/original/competencia-profesional/mercancias/preguntas/{file_name}.txt'
            file_data = _read_file(input_path)
            with doc.create(Subsection(file_name)):
                questions = parse_questions(file_data)
                add_questions(doc, questions['questions'])
                questions = json.dumps(questions["questions"],indent=1)
                output_path = f'data/current/competencia-profesional/mercancias/preguntas/{file_name}.json'
                _write_output(output_path, questions)

def parse_competencia_professional_mercancias_practical_questions():
    date = '04-03-2022'
    letters = ['A','B','C','D','E','F','G','H']
    for letter in letters:
        file_name = f'cm1{letter}'
        input_path = f'data/{date}/original/competencia-profesional/mercancias/practicos/{file_name}.txt'
        file_data = _read_file(input_path)
        questions = parse_questions(file_data)
        questions = json.dumps(questions["questions"],indent=1)
        output_path = f'data/current/competencia-profesional/mercancias/practicos/{file_name}.json'
        _write_output(output_path, questions)

def parse_competencia_professional_viajeros_questions():
    date = '04-03-2022'
    letters = ['A','B','C','D','E','F','G','H']
    for letter in letters:
        file_name = f'pv2{letter}'
        input_path = f'data/{date}/original/competencia-profesional/viajeros/preguntas/{file_name}.txt'
        file_data = _read_file(input_path)
        questions = parse_questions(file_data)
        questions = json.dumps(questions["questions"],indent=1)
        output_path = f'data/current/competencia-profesional/viajeros/preguntas/{file_name}.json'
        _write_output(output_path, questions)



def parse_competencia_professional_viajeros_practical_questions():
    date = '04-03-2022'
    letters = ['A','B','C','D','E','F']
    for letter in letters:
        file_name = f'cv2{letter}'
        input_path = f'data/{date}/original/competencia-profesional/viajeros/practicos/{file_name}.txt'
        file_data = _read_file(input_path)
        questions = parse_questions(file_data)
        questions = json.dumps(questions["questions"],indent=1)
        output_path = f'data/current/competencia-profesional/viajeros/practicos/{file_name}.json'
        _write_output(output_path, questions)


def parse_conductores_professionales_questions(doc):
    date = '04-03-2022'
    job='conductores-professionales'
    input_base_path  = f'data/{date}/original/{job}'
    output_base_path =  f'data/current/{job}'
    for folder in os.listdir(input_base_path):
        output_path = output_base_path +'/'+ folder
        # print(output_path)
        input_path = input_base_path+'/'+ folder
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        for file in os.listdir(input_path):
            file_path = input_path+'/'+file
            output_file_path = output_path + '/' + (file.split('.txt')[0]) + '.json'
            file_data = _read_file(file_path)
            questions = parse_questions(file_data)
            add_questions(doc, questions['questions'])
            questions = json.dumps(questions["questions"],indent=1)
            _write_output(output_file_path, questions)
    # for letter in letters:
    #     file_name = f'cv2{letter}'
        
    #     file_data = _read_file(input_path)
    #     questions = parse_questions(file_data)
    #     questions = json.dumps(questions["questions"],indent=1)
    #     output_path = f'data/current/competencia-profesional/viajeros/practicos/{file_name}.json'
    #     _write_output(output_path, questions)



def parse_consejero_seguridad_questions(doc):
    date = '04-03-2022'
    letters = [1,2,3,4,5]
    for letter in letters:
        file_name = f'PCS{letter}'
        input_path = f'data/{date}/original/consejero-seguridad/preguntas/{file_name}.txt'
        try: 
            file_name = f'PCS{letter}'
            input_path = f'data/{date}/original/consejero-seguridad/preguntas/{file_name}.txt'
            file_data = _read_file(input_path,encoding='latin1')
            questions = parse_questions(file_data)
            questions = json.dumps(questions["questions"],indent=1)
            output_path = f'data/current/consejero-seguridad/preguntas/{file_name}.json'
            _write_output(output_path, questions)
        except:
            print(input_path)

def parse_consejero_seguridad_practical_questions():
    date = '04-03-2022'
    letters = [1,2,3,4,5]
    for letter in letters:
        file_name = f'CCS{letter}'
        input_path = f'data/{date}/original/consejero-seguridad/practicos/{file_name}.txt'
        file_data = _read_file(input_path,encoding='latin1')
        questions = parse_questions(file_data)
        questions = json.dumps(questions["questions"],indent=1)
        output_path = f'data/current/consejero-seguridad/practicos/{file_name}.json'
        _write_output(output_path, questions)

def _config_document():
    document_class = Command('documentclass', options=Options('12pt', 'a4paper'), arguments='article')  
    doc = Document(documentclass=document_class)
    doc.append(Command('tableofcontents'))
    # doc.preamble.append(header)

    return doc

def export_to_pdf(doc):
    doc.generate_pdf('basic_maketitle2', clean_tex=False)

current_root_foolder = 'data/04-03-2022/original'
new_root_foolder = 'data/14-04-2023/original'

files_collection = [
    # {
    # 'base': 'data/04-03-2022/original/competencia-profesional/mercancias/preguntas',
    # 'job': 'Competencia professional: Mercancias',
    # 'Subsection': 'Preguntas',
    # 'keys': ["A", "B", "C", "D"], 
    # 'files': [
    #     { 'name': 'pm1B.txt', 'title': 'Elementos de Derecho civil'},
    #     { 'name': 'pm1A.txt', 'title': 'Elementos de Derecho mercantil'} ,
    #     { 'name': 'pm1C.txt', 'title': 'Elementos de Derecho laboral'},
    #     { 'name': 'pm1D.txt', 'title': 'Elementos de Derecho fiscal'},
    #     { 'name': 'pm1E.txt', 'title': 'Gestión comercial y financiera de la empresa'},
    #     { 'name': 'pm1F.txt', 'title': 'Acceso a los mercados'},
    #     { 'name': 'pm1G.txt', 'title': 'Normas y explotación técnicas'},
    #     { 'name': 'pm1H.txt', 'title': 'Seguridad vial'}
    # ]
    # },
    # {
    # 'base': 'data/04-03-2022/original/competencia-profesional/mercancias/practicos',
    # 'job': 'Competencia professional: Mercancias',
    # 'Subsection': 'Practicos',
    # 'keys': ["A", "B", "C", "D"], 
    # 'files': [
    #     { 'name': 'cm1B.txt', 'title': 'Elementos de Derecho civil'},
    #     { 'name': 'cm1A.txt', 'title': 'Elementos de Derecho mercantil'} ,
    #     { 'name': 'cm1C.txt', 'title': 'Elementos de Derecho laboral'},
    #     { 'name': 'cm1D.txt', 'title': 'Elementos de Derecho fiscal'},
    #     { 'name': 'cm1E.txt', 'title': 'Gestión comercial y financiera de la empresa'},
    #     { 'name': 'cm1F.txt', 'title': 'Acceso a los mercados'},
    #     { 'name': 'cm1G.txt', 'title': 'Normas y explotación técnicas'},
    #     { 'name': 'cm1H.txt', 'title': 'Seguridad vial'}
    # ]
    # },
    # {
    # 'base': 'data/04-03-2022/original/competencia-profesional/viajeros/practicos',
    # 'job': 'Competencia professional: Viajeros',
    # 'Subsection': 'Preguntas',
    # 'keys': ["A", "B", "C", "D"], 
    # 'files': [
    #     { 'name': 'cv2A.txt', 'title': 'Elementos de Derecho mercantil'} ,
    #     { 'name': 'cv2B.txt', 'title': 'Elementos de Derecho civil'},
    #     { 'name': 'cv2C.txt', 'title': 'Elementos de Derecho laboral'},
    #     { 'name': 'cv2D.txt', 'title': 'Elementos de Derecho fiscal'},
    #     { 'name': 'cv2E.txt', 'title': 'Gestión comercial y financiera de la empresa'},
    #     { 'name': 'cv2F.txt', 'title': 'Acceso a los mercados'},
    # ]
    # },
    # {
    # 'base': 'data/04-03-2022/original/competencia-profesional/viajeros/preguntas',
    # 'job': 'Competencia professional: Viajeros',
    # 'Subsection': 'Preguntas',
    # 'keys': ["A", "B", "C", "D"], 
    # 'files': [
    #     { 'name': 'pv2A.txt', 'title': 'Elementos de Derecho mercantil'} ,
    #     { 'name': 'pv2B.txt', 'title': 'Elementos de Derecho civil'},
    #     { 'name': 'pv2C.txt', 'title': 'Elementos de Derecho laboral'},
    #     { 'name': 'pv2D.txt', 'title': 'Elementos de Derecho fiscal'},
    #     { 'name': 'pv2E.txt', 'title': 'Gestión comercial y financiera de la empresa'},
    #     { 'name': 'pv2F.txt', 'title': 'Acceso a los mercados'},
    #     { 'name': 'pv2G.txt', 'title': 'Normas y explotación técnicas'},
    #     { 'name': 'pv2H.txt', 'title': 'Seguridad vial'}
    # ]
    # },
    # {
    # 'base': 'data/04-03-2022/original/conductores-professionales/Seccion1',
    # 'job': 'Conductores Professionales',
    # 'Subsection': 'Seccion 1',
    # 'keys': ["A", "B", "C", "D"], 
    # 'files': [
    #     { 'name': '1.1.txt', 'title': 'Objetivo 1.1'} ,
    #     { 'name': '1.2.txt', 'title': 'Objetivo 1.2'},
    #     { 'name': '1.3.txt', 'title': 'Objetivo 1.3'},
    #     { 'name': '1.3BIS.txt', 'title': 'Objetivo 1.3 bis'} ,
    #     { 'name': '2.1.txt', 'title': 'Objetivo 2.1'},
    #     { 'name': '3.1.txt', 'title': 'Objetivo 3.1'},
    #     { 'name': '3.2.txt', 'title': 'Objetivo 3.2'},
    #     { 'name': '3.3.txt', 'title': 'Objetivo 3.3'},
    #     { 'name': '3.4.txt', 'title': 'Objetivo 3.4'},
    #     { 'name': '3.5.txt', 'title': 'Objetivo 3.5'},
    #     { 'name': '3.6.txt', 'title': 'Objetivo 3.6'},
    #     { 'name': 'V5.txt', 'title': 'V5'}

    # ]
    # },
    {
    'base': f'{current_root_foolder}/conductores-professionales/Seccion2',
    'new': f'{new_root_foolder}/conductores-professionales/Seccion 2',
    'job': 'Conductores Professionales',
    'Subsection': 'Seccion 2',
    'keys': ["A", "B", "C", "D"], 
    'files': [
        { 'name': '1.4.txt', 'title': 'Objetivo 1.4'} ,
        { 'name': '2.2.txt', 'title': 'Objetivo 2.2'},
        { 'name': '3.7.txt', 'title': 'Objetivo 3.7'}
    ]
    },

    
    #     {
    # 'base': 'data/04-03-2022/original/conductores-professionales/Seccion3',
    # 'job': 'Conductores Professionales',
    # 'Subsection': 'Seccion 3',
    # 'keys': ["A", "B", "C", "D"], 
    # 'files': [
    #     { 'name': '1.5.txt', 'title': 'Objetivo 1.5'} ,
    #     { 'name': '1.6.txt', 'title': 'Objetivo 1.6'},
    #     { 'name': '2.3.txt', 'title': 'Objetivo 2.3'},
    #     { 'name': '3.8.txt', 'title': 'Objetivo 3.8'}
    # ]
    # },
    #     {
    # 'base': 'data/04-03-2022/original/conductores-professionales/Seccion4',
    # 'job': 'Conductores Professionales',
    # 'Subsection': 'Seccion 4',
    # 'keys': ["A", "B", "C", "D"], 
    # 'files': [
    #     { 'name': 'M1.txt', 'title': 'Módulo 1'},
    #     { 'name': 'M2.txt', 'title': 'Módulo 2'},
    #     { 'name': 'M3.txt', 'title': 'Módulo 3'},
    #     { 'name': 'M4.txt', 'title': 'Módulo 4'},
    #     { 'name': 'M5.txt', 'title': 'Módulo 5'},
    #     { 'name': 'M6.txt', 'title': 'Módulo 6'},
    #     { 'name': 'M7.txt', 'title': 'Módulo 7'},
    #     { 'name': 'M8.txt', 'title': 'Módulo 8'},
    #     { 'name': 'M9.txt', 'title': 'Módulo 9'}
    # ]
    # },
    #     {
    # 'base': 'data/04-03-2022/original/conductores-professionales/Seccion4;5',
    # 'job': 'Conductores Professionales',
    # 'Subsection': 'Seccion 4;5',
    # 'keys': ["A", "B", "C", "D"], 
    # 'files': [
    #     { 'name': 'M7V4.txt', 'title': 'Formación sobre sensibilización y educación vial.'} ,
    #     { 'name': 'M8V5.txt', 'title': 'Formación sobre el tacógrafo digital.'}
    # ]
    # },
    #     {
    # 'base': 'data/04-03-2022/original/conductores-professionales/Seccion5',
    # 'job': 'Conductores Professionales',
    # 'Subsection': 'Seccion 5',
    # 'keys': ["A", "B", "C", "D"], 
    # 'files': [
    #     { 'name': 'V1.txt', 'title': 'Módulo 1. Formación en materia de sensibilización sobre los derechos de los viajeros con discapacidad de conformidad con el Reglamento (UE) n.º 181/2011 del Parlamento Europeo y del Consejo.'} ,
    #     { 'name': 'V2.txt', 'title': 'Módulo 2. Formación sobre el transporte escolar y de menores.'},
    #     { 'name': 'V3.txt', 'title': 'Módulo 3. Formación en primeros auxilios.'},
    #     { 'name': 'V4.txt', 'title': 'Módulo 4. Formación sobre sensibilización y educación vial.'} ,
    #     { 'name': 'V5.txt', 'title': 'Módulo 5. Formación sobre el tacógrafo digital.'},
    #     { 'name': 'V6.txt', 'title': 'Módulo 6. Atención al cliente de transporte de viajeros.'},
    #     { 'name': 'V7.txt', 'title': 'Módulo 7. Atención e información a los viajeros del autobús.'} ,
    #     { 'name': 'V8.txt', 'title': 'Módulo 8. Protocolo de actuación para conductores ante un accidente.'},
    # ]
    # },
    # {
    # 'base': 'data/04-03-2022/original/consejero-seguridad/preguntas',
    # 'job': 'Consejero Seguridad',
    # 'Subsection': 'Preguntas',
    # 'keys': ["A", "B", "C", "D"], 
    # 'files': [
    #     { 'name': 'PCS1.txt', 'title': 'Clase 1: Materias y objetos explosivos '} ,
    #     { 'name': 'PCS2.txt', 'title': 'Clase 2: Gases'},
    #     { 'name': 'PCS3.txt', 'title': 'Clase 7: Materias radiactivas'},
    #     { 'name': 'PCS4.txt', 'title': 'Clases 3; 4.1; 4.2; 4.3; 5.1; 5.2; 6.1; 6.2; 8 y 9: (Resto de Clases)'},
    #     { 'name': 'PCS5.txt', 'title': 'Materias líquidas inflamables con los números de identificación de la Organización de Naciones Unidas UN 1202 (Gasóleo), UN 1203 (Gasolina), UN 1223 (Queroseno), UN 3475 (Mezcla de etanol y gasolina o Mezcla de etanol y combustible para motores) y los combustibles de aviación con los números UN 1268 y UN 1863: (Productos petrolíferos)'},
    # ]
    # },
    # {
    # 'base': 'data/04-03-2022/original/consejero-seguridad/practicos',
    # 'job': 'Consejero Seguridad',
    # 'Subsection': 'Practicos',
    # 'keys': ["A", "B", "C", "D"], 
    # 'files': [
    #     { 'name': 'CCS1.txt', 'title': 'Elementos de Derecho mercantil'} ,
    #     { 'name': 'CCS2.txt', 'title': 'Elementos de Derecho civil'},
    #     { 'name': 'CCS3.txt', 'title': 'Elementos de Derecho laboral'},
    #     { 'name': 'CCS4.txt', 'title': 'Elementos de Derecho fiscal'},
    #     { 'name': 'CCS5.txt', 'title': 'Gestión comercial y financiera de la empresa'},
    # ]
    # }
    
]


def _process_files(files):
    current_doc = Document('Basic')
    current_doc.append(Section(files['job'] + ' ' + files['Subsection']))
    new_doc = Document('Basic')
    new_doc.append(Section(files['job'] + ' ' + files['Subsection']))
    diferences_doc = Document('Basic')
    diferences_doc.append(Section(files['job'] + ' ' + files['Subsection']))
    for f in files['files']:
        try: 
            current_path = files['base'] + '/' + f['name']
            new_path = files['new'] + '/' + f['name']
            current_exam = Exam( source_file_path=current_path , keys=files['keys'], title=f['title'], pdf_name=f['title'])
            current_exam.toSubSection(current_doc)
            new_exam = Exam( source_file_path=new_path , keys=files['keys'], title=f['title'], pdf_name=f['title'])
            new_exam.toSubSection(new_doc)
            differences = new_exam.search_diferences(current_exam)
            differences.toSubSectionComparative(diferences_doc)
        except Exception as e:
            print('Error: ' + files['base'] + '/' + f['name'])
            print(e)
    current_doc.generate_pdf('Output/' + files['job'] + ' ' + files['Subsection'])
    new_doc.generate_pdf('Output/' + files['job'] + ' ' + files['Subsection'] + ' ' + 'Versió Nova')
    diferences_doc.generate_pdf('Output/' + files['job'] + ' ' + files['Subsection'] + ' ' + 'Diferencies')
    print('file generated:' +  files['job'] + ' ' + files['Subsection'])


for files in files_collection: 
    _process_files(files)


