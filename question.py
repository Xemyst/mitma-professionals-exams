import json
from enums.question_type import QuestionType
from pylatex import Document, Section, Subsection, Command, LineBreak
from pylatex import Document, Section, Subsection, Command, LineBreak
from pylatex.utils import italic, NoEscape, bold
from pylatex.base_classes.command import Options

class Question:
    def __init__(self, question_data: dict, question_type: QuestionType):
        self.code = question_data['code']
        self.question = question_data['question']
        self.solution = question_data['solution']
        self.question_type = question_type
        if(question_type == QuestionType.COMPETENCIA_PROFESIONAL_PREGUNTAS or question_type == QuestionType.CONDUCTORES_PROFESSIONALES):
            self.a = question_data['a']
            self.b = question_data['b']
            self.c = question_data['c']
            self.d = question_data['d']
            self.rule = question_data['rule']
            if(question_type == QuestionType.CONDUCTORES_PROFESSIONALES):
                self.doctrinal_reference = question_data['doctrinal_reference']
        elif(question_type == QuestionType.COMPETENCIA_PROFESIONAL_PRACTICOS):
            self.a = question_data['a']
            self.b = question_data['b']
            self.c = question_data['c']
            self.d = question_data['d']
            self.e = question_data['e']
            self.f = question_data['f']
            self.g = question_data['g']
            self.h = question_data['h']
            self.rule = question_data['rule']
        elif(question_type == QuestionType.CONSEJERO_SEGURIDAD_PRACTICOS):
            pass
        elif(question_type == QuestionType.CONSEJERO_SEGURIDAD_PREGUNTAS):
            self.a = question_data['1']
            self.b = question_data['2']
            self.c = question_data['3']
            self.d = question_data['4']
            self.rule = question_data['rule']

    def __str__(self) -> str:
        return f'"cod": "{self.code}", "question" : "{self.question}", "a" : "{self.a}", "b" : "{self.b}", "c": "{self.c}", "d": "{self.d}", "answer" : "{self.solution}" '
    
    def __eq__(self, __o: object) -> bool:
        if (type (self) != type (__o)): return False
        return True
    
    def toJson(self):
        return  json.dumps({ "cod": self.code,
            "question" : self.question, 
            "a" : self.a, 
            "b" : self.b, 
            "c": self.c,
            "d": self.d, 
            "answer" : self.answer
        },  ensure_ascii=False,indent=0)
    

    def insertQuestionToLatex(self, doc: Document):
        self._add_key_value(doc,"CODE: ", self.code )
        self._add_value(doc, self.question)
        self._insert_options(doc)
        self._add_key_value(doc, "Solucio: ", self.solution)
        self._add_insert_justification(doc)

    def _add_insert_justification(self, doc:Document):
        question_type = self.question_type
        if(question_type == QuestionType.COMPETENCIA_PROFESIONAL_PREGUNTAS or question_type == QuestionType.COMPETENCIA_PROFESIONAL_PRACTICOS):
            self._add_key_value(doc,"Norma: ", self.rule )
        elif(question_type == QuestionType.CONSEJERO_SEGURIDAD_PRACTICOS):
            pass
        elif(question_type == QuestionType.CONSEJERO_SEGURIDAD_PREGUNTAS):
            pass
        elif(question_type == QuestionType.CONDUCTORES_PROFESSIONALES):
            self._add_key_value(doc,"Norma: ", self.rule )
            self._add_key_value(doc,"Referencia Doctrinal: ", self.doctrinal_reference )


    def _insert_options(self, doc: Document):
        question_type = self.question_type
        if(question_type == QuestionType.COMPETENCIA_PROFESIONAL_PREGUNTAS or question_type == QuestionType.CONDUCTORES_PROFESSIONALES):
            self._add_key_value(doc,"A) ", self.a )
            self._add_key_value(doc,"B) ", self.b )
            self._add_key_value(doc,"C) ", self.c )
            self._add_key_value(doc,"D) ", self.d )
        elif(question_type == QuestionType.COMPETENCIA_PROFESIONAL_PRACTICOS):
            self._add_key_value(doc,"RESPUESTA A:\n", self.a )
            self._add_key_value(doc,"RESPUESTA B:\n ", self.b )
            self._add_key_value(doc,"RESPUESTA C:\n ", self.c )
            self._add_key_value(doc,"RESPUESTA D:\n ", self.d )
            self._add_key_value(doc,"RESPUESTA E:\n ", self.e )
            self._add_key_value(doc,"RESPUESTA F:\n ", self.f )
            self._add_key_value(doc,"RESPUESTA G:\n ", self.g )
            self._add_key_value(doc,"RESPUESTA H:\n ", self.h )
        elif(question_type == QuestionType.CONSEJERO_SEGURIDAD_PRACTICOS):
            pass
        elif(question_type == QuestionType.CONSEJERO_SEGURIDAD_PREGUNTAS):
            self._add_key_value(doc,"1) ", self.a )
            self._add_key_value(doc,"2) ", self.b )
            self._add_key_value(doc,"3) ", self.c )
            self._add_key_value(doc,"4) ", self.d )

    def _add_key_value(self, doc, key,value):
        doc.append(bold(key))
        doc.append(value)
        doc.append("\n")

    def _add_value(self, doc, value):
        doc.append(value)
        doc.append("\n")

    def _compare_conductores_professionales_question(self, question):
        a = self.a == question.a 
        b = self.b == question.b 
        c = self.c == question.c 
        d = self.d == question.d 
        solution = self.solution == question.solution 
        rule = self.rule == question.rule 
        doctrinal_reference = self.doctrinal_reference == question.doctrinal_reference
        return (a and b and c and d and solution and rule and doctrinal_reference)

    def equals(self,question,type):
        if type == QuestionType.CONDUCTORES_PROFESSIONALES:
            return self._compare_conductores_professionales_question(question)