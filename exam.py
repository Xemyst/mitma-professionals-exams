from question_parser import QuestionParser
from pylatex import Document, Section, Subsection, Command, LineBreak, Subsubsection
from pylatex.utils import italic, NoEscape, bold
from pylatex.base_classes.command import Options

class Exam:
    def __init__(self, source_file_path = '', keys = '', source_type = 'raw',date = 'today', encoding='utf-8', title="untitled", pdf_name = 'output'):
        if source_type == 'raw':
            self._process_raw_input_file(source_file_path, encoding=encoding)
        self.title = title
        self.keys = keys 
        self.pdf_name = pdf_name

    # def __str__(self) -> str:
    #     return f'"cod": "{self.code}", "question" : "{self.question}", "a" : "{self.a}", "b" : "{self.b}", "c": "{self.c}", "d": "{self.d}", "answer" : "{self.answer}" '
    
    def __eq__(self, __o: object) -> bool:
        if (type (self) != type (__o)): return False
        return True
    
    def toJson(self):
        return  json.dumps({ "cod": self.code,
            "question" : self.question, 
            "a" : self.a, 
            "b" : self.b, 
            "c": self.c,
            "d": self.d, 
            "answer" : self.answer
        },  ensure_ascii=False,indent=0)
    
    def toPdf(self, clean_tex = True): 
        doc = self._config_document()
        doc.append(Section(self.title))
        self._add_questions(doc)
        doc.generate_pdf(self.pdf_name, clean_tex=clean_tex)
        # add subsection

    def toPdfFrom(self, clean_tex = True): 
        doc = self._config_document()
        doc.append(Section(self.title))
        self._add_questions(doc)
        doc.generate_pdf(self.pdf_name, clean_tex=clean_tex)
        # add subsection

    def toSubSectionComparative(self, doc: Document, clean_tex = True): 
        doc.append(Subsection(self.title))
        doc.append(Subsubsection('Preguntas nuevas'))
        self._add_new_questions(doc)  
        doc.append(Subsubsection('Preguntas Eliminadas'))
        self._add_deleted_questions(doc)
        doc.append(Subsubsection('Preguntas Modificadas'))
        self._add_modified_questions(doc)  

    def _add_new_questions(self, doc):
        for question in self.new_questions:
            question.insertQuestionToLatex(doc)

    def _add_deleted_questions(self,doc):
        for question in self.deleted_questions:
            question.insertQuestionToLatex(doc)

    def _add_modified_questions(self,doc):
        for question in self.modified_questions:
            question.insertQuestionToLatex(doc)          

    def toSubSection(self, doc: Document, clean_tex = True): 
        doc.append(Subsection(self.title))
        self._add_questions(doc) 

    def export_as_subsection(self, doc):
        doc.append(Subsection(self.title))
        self._add_questions(doc)

    def _add_questions(self, doc: Document):
        for question in self.questions:
            question.insertQuestionToLatex(doc)
            doc.append("\n")

    def _add_key_value(self, doc, key,value):
        doc.append(bold(key))
        doc.append(value)
        doc.append("\n")

    def _add_value(self, doc, value):
        doc.append(value)
        doc.append("\n")

    def _add_question(self, doc, question):
        self._add_key_value(doc,"CODE: ", question["code"] )
        self._add_value(doc, question["question"])

        for option in self.keys:
            self._add_key_value(doc, f'${option} ', question[option])

        self._add_key_value(doc,"Solució: ", question["solution"] )
        # _add_key_value(doc,"Norma: ", question["NORMA"] )
        doc.append("\n")

    def _process_raw_input_file(self,input_path, encoding='utf-8'):
        file_data = self._read_file(input_path, encoding=encoding)
        question_information = self.parse_questions(file_data)
        print(question_information["question_type"])
        self.type = question_information["question_type"]
        self.questions = question_information["questions"]
        # self.options =  question_information["options"]
        

    def parse_questions(self, input_data):
        qp = QuestionParser()
        return qp.parse_questions(input_data);

    def _read_file(self, path, encoding = 'utf-8') -> str: 
        file = open(path, 'r', encoding=encoding)
        file_data = file.read()
        file.close()
        return file_data

    def _config_document(self):
        document_class = Command('documentclass', options=Options('12pt', 'a4paper'), arguments='article')  
        doc = Document(documentclass=document_class)
        # doc.preamble.append(header)
        return doc

    def search_diferences(self, previous_exam):
        new_exam = Exam(title=self.title, pdf_name=self.pdf_name, source_type='exam')

        new_questions = self._find_new_questions(previous_exam)
        deleted_questions = self._find_deleted_questions(previous_exam)
        modified_questions = self._find_modified_questions(previous_exam)

        new_exam.new_questions = new_questions
        new_exam.deleted_questions = deleted_questions
        new_exam.modified_questions = modified_questions

        return new_exam

    def find_question_by_id(self, code):
        # print(self.questions)
        for question in self.questions:
            if(question.code == code):
                return question

    def _find_new_questions(self,previous_exam):
        new_questions = []
        for question in self.questions:
            # print(previous_exam.find_question_by_id(question.code) == None)
            if(previous_exam.find_question_by_id(question.code) == None):
                new_questions += [question]
        return new_questions
    
    def _find_deleted_questions(self,previous_exam):
        deleted_questions = []
        for question in previous_exam.questions:
            # print(previous_exam.find_question_by_id(question.code) == None)
            if(self.find_question_by_id(question.code) == None):
                deleted_questions += [question]
        return deleted_questions
    
    def _find_modified_questions(self, previous_exam):
        modified_questions = []
        for question in self.questions:
            q = previous_exam.find_question_by_id(question.code)
            if(q != None):
                if not question.equals(q, self.type):
                   modified_questions += [question]

        return modified_questions