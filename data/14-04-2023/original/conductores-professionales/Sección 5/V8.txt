COD: 262

¿Qué debemos hacer ante una hemorragia en la pierna?

A Presionar la arteria humeral.

B Presionar la arteria femoral.

C Elevar el brazo.

D Presionar una pierna con la otra.

RESPUESTA: B

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 2546

En relación con el auxilio a los heridos de un accidente de tráfico, señale cuál de las siguientes afirmaciones es incorrecta.

A Si, una vez abierta y limpia la vía aérea, tras 10 segundos comprobamos que sigue sin respirar, practicaremos la respiración artificial.

B En caso de no tener pulso, se debe efectuar la respiración artificial.

C Nunca debemos actuar nosotros ante una hemorragia, debe hacerlo solo personal especializado.

D Si hay signos de traumatismo por encima de la clavícula, no se debe mover el cuello del accidentado.

RESPUESTA: C

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 3422

¿Cuál es la posición en la que debemos colocar a un herido que pensamos que puede estar sufriendo un shock hipovolémico?

A Boca arriba.

B Boca abajo.

C Con la cabeza por debajo del resto del cuerpo.

D Con la cabeza por encima del resto del cuerpo.

RESPUESTA: C

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 5035

¿Cómo se debe colocar la mano para realizar la respiración artificial?

A Sobre la frente para empujarla hacia atrás.

B Bajo la nuca para empujar hacia abajo.

C Sobre la frente para empujar hacia delante.

D Bajo la nuca para empujar hacia arriba.

RESPUESTA: A

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 5221

¿A qué tipo de heridos no hay que moverles el cuello?

A A los que tienen fracturas de pelvis.

B A los heridos sin pulso.

C A los quemados.

D A los que dicen que no pueden mover alguna parte de su cuerpo.

RESPUESTA: D

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66246

Si un conductor de autobús se encuentra en la carretera con un accidente de tráfico, ¿está obligado a parar y ayudar a las víctimas?

A No, salvo que su vehículo se vea implicado en el accidente.

B Solo si no lleva pasajeros.

C Sí.

D No está legalmente obligado, pero es una obligación moral.

RESPUESTA: C

NORMA: RD Legislativo 6/2015, art. 51

REFERENCIA DOCTRINAL: Sin referencias doctrinales



COD: 66247

En un accidente de tráfico con heridos graves:

A No se debe intervenir nunca sin presencia policial para no alterar las pruebas.

B Se debe ayudar a los heridos, pero siempre sin moverlos de donde estén para no alterar las cosas.

C Se evitará modificar las cosas, salvo que se perjudique la seguridad de los heridos.

D Se puede actuar sin ninguna precaución especial, salvo que  haya muertos.

RESPUESTA: C

NORMA: RD 1428/2003, art. 129

REFERENCIA DOCTRINAL: Sin referencias doctrinales



COD: 66248

¿Hay que avisar a los agentes de la autoridad si nos vemos implicados en un accidente de tráfico?

A Sí, cuando haya muertos o heridos graves.

B Solo es obligatorio cuando haya muertos.

C Sí, es siempre obligatorio.

D No es obligatorio, pero se recomienda hacerlo para que garanticen la seguridad del tráfico en la zona.

RESPUESTA: A

NORMA: RD 1428/2003, art. 129

REFERENCIA DOCTRINAL: Sin referencias doctrinales



COD: 66249

¿Hay que permanecer en el lugar donde se haya producido un accidente de tráfico con heridos graves en el que estamos implicados?

A Siempre.

B No es obligatorio si dejamos nuestros datos de identificación y de contacto.

C Sí, salvo que los agentes de la autoridad nos permitan marcharnos o dejemos el lugar para atender a heridos o para ser nosotros atendidos. 

D No, cuando nuestro vehículo no haya sufrido daños importantes.

RESPUESTA: C

NORMA: RD 1428/2003, art. 129

REFERENCIA DOCTRINAL: Sin referencias doctrinales



COD: 66250

¿Hay que permanecer en el lugar donde se haya producido un accidente de tráfico con heridos leves en el que estamos implicados?

A Siempre.

B No, cuando la seguridad de la circulación esté restablecida y ninguno de los implicados lo solicite.

C No es obligatorio nunca si dejamos nuestros datos de identificación y de contacto.

D No, cuando nuestro vehículo no haya sufrido daños importantes.

RESPUESTA: B

NORMA: RD 1428/2003, art. 129

REFERENCIA DOCTRINAL: Sin referencias doctrinales



COD: 66251

¿Es obligatorio comunicar nuestra identidad si nos vemos implicados en un accidente de tráfico?

A Sí, a todos los demás implicados.

B Sí, a los restantes implicados que lo soliciten.

C Solo a los agentes de seguridad.

D No, basta con facilitar la matrícula del vehículo.

RESPUESTA: B

NORMA: RD 1428/2003, art. 129

REFERENCIA DOCTRINAL: Sin referencias doctrinales



COD: 66252

¿Es obligatorio comunicar los datos de nuestro vehículo si nos vemos implicados en un accidente de tráfico?

A Sí, a todos los demás implicados.

B Sí, a los restantes implicados que lo soliciten.

C Solo a los agentes de seguridad.

D No, basta con facilitar nuestra identidad.

RESPUESTA: B

NORMA: RD 1428/2003, art. 129

REFERENCIA DOCTRINAL: Sin referencias doctrinales



COD: 66253

En caso de accidente de tráfico, el Código Penal considera delito:

A la omisión del deber de socorro.

B el abandono del lugar del accidente, aunque no se haya faltado al deber de socorro, cuando se sea causa del accidente y haya heridos.

C el abandono del lugar del accidente, aunque no se haya faltado al deber de socorro, en todos los casos.

D Las respuestas A y B son correctas.

RESPUESTA: D

NORMA: Ley Orgánica 10/1995, art. 195; Ley Orgánica 10/1995, 382 bis

REFERENCIA DOCTRINAL: Sin referencias doctrinales



COD: 66254

En caso de accidente de tráfico, incurre en el delito de omisión del deber de socorro:

A el que no ayude a un afectado que se encuentre en peligro grave y manifiesto, pudiendo hacerlo sin riesgo propio ni de terceros.

B el que no ayude a cualquier afectado por el accidente, pudiendo hacerlo sin riesgo propio ni de terceros.

C el que no ayude a un afectado por el accidente, pero solo si él ha sido causa de dicho accidente.

D el que no ayude a un afectado por el accidente, pero solo si él ha estado implicado en dicho accidente.

RESPUESTA: A

NORMA: Ley Orgánica 10/1995, art. 195

REFERENCIA DOCTRINAL: Sin referencias doctrinales



COD: 66255

En caso de accidente de tráfico, incurre en el delito de omisión del deber de socorro:

A el que no ayude a un afectado que se encuentre en peligro grave y manifiesto, pudiendo hacerlo sin riesgo propio ni de terceros.

B el que, en caso de peligro grave y manifiesto, no solicite ayuda ajena con urgencia si no puede prestarla personalmente.

C el que no ayude a un afectado por el accidente o no solicite ayuda ajena con urgencia, pero solo si él ha sido causa de dicho accidente.

D Las respuestas A y B son correctas.

RESPUESTA: D

NORMA: Ley Orgánica 10/1995, art. 195

REFERENCIA DOCTRINAL: Sin referencias doctrinales



COD: 66256

En caso de accidente de tráfico, incurre en el delito de abandono del lugar del accidente:

A quien, habiendo presenciado el accidente, abandone el lugar si no había riesgo de permanecer en él.

B quien, habiendo presenciado el accidente, abandone el lugar, pero solo si hay heridos y no había riesgo de permanecer allí.

C quien, habiendo causado el accidente, abandone el lugar, pero solo si hay heridos y no había riesgo de permanecer allí.

D Las respuestas B y C son correctas.

RESPUESTA: C

NORMA: Ley Orgánica 10/1995, 382 bis

REFERENCIA DOCTRINAL: Sin referencias doctrinales



COD: 66257

En la atención a heridos en accidente de tráfico la regla general es:

A sacarlos cuanto antes del vehículo para que puedan estar recostados o tumbados.

B sacarlos cuanto antes del vehículo si se trata de ancianos o personas con movilidad reducida.

C no moverlos, salvo que esté en peligro su vida.

D no moverlos en ningún caso para no provocarles una lesión de médula.

RESPUESTA: C

NORMA: Programa de Intervención, Sensibilización y Reeducación Vial - Manual del formador. Dirección General de Tráfico - INTRAS

REFERENCIA DOCTRINAL: Programa de Intervención, Sensibilización y Reeducación Vial - Manual del formador. Dirección General de Tráfico - INTRAS



COD: 66258

¿Qué implica mantener el eje cabeza-cuello-tronco al mover a un herido en accidente de tráfico?

A Que no pueden ser movidos en ningún caso.

B Que para moverlos hay que evitar desplazamientos de la cabeza o del cuello respecto del tronco.

C Que para moverlos hay que hacerlo en bloque.

D Las respuestas B y C son correctas.

RESPUESTA: D

NORMA: Programa de Intervención, Sensibilización y Reeducación Vial - Manual del formador. Dirección General de Tráfico - INTRAS

REFERENCIA DOCTRINAL: Programa de Intervención, Sensibilización y Reeducación Vial - Manual del formador. Dirección General de Tráfico - INTRAS



COD: 66259

¿Por qué hay que mantener el eje cabeza-cuello-tronco al mover a un herido en accidente de tráfico?

A Para que no vomiten.

B Para no provocarles lesiones de médula.

C Para que no se mareen.

D Para que sangren lo menos posible.

RESPUESTA: B

NORMA: Programa de Intervención, Sensibilización y Reeducación Vial - Manual del formador. Dirección General de Tráfico - INTRAS

REFERENCIA DOCTRINAL: Programa de Intervención, Sensibilización y Reeducación Vial - Manual del formador. Dirección General de Tráfico - INTRAS



COD: 66260

En la atención a heridos en accidente de tráfico que viajaban en moto la regla general es:

A siempre quitarles cuanto antes el casco para que puedan respirar mejor.

B quitarles cuanto antes el casco, si este se ha roto.

C no quitarles el casco, salvo que sea imprescindible para atenderlos.

D no quitarles el casco en ningún caso para no provocarles una lesión de médula.

RESPUESTA: C

NORMA: Programa de Intervención, Sensibilización y Reeducación Vial - Manual del formador. Dirección General de Tráfico - INTRAS

REFERENCIA DOCTRINAL: Programa de Intervención, Sensibilización y Reeducación Vial - Manual del formador. Dirección General de Tráfico - INTRAS



COD: 66261

Llevar un botiquín de primeros auxilios con material en condiciones de uso:

A es recomendable para todos los vehículos, aunque no obligatorio.

B es obligatorio para los autobuses de más de 25 plazas, incluido el conductor.

C es obligatorio para todos los autobuses.

D es obligatorio para todos los vehículos.

RESPUESTA: A

NORMA: Manual de primeros auxilios para profesores de formación vial. Dirección General de Tráfico

REFERENCIA DOCTRINAL: Manual de primeros auxilios para profesores de formación vial. Dirección General de Tráfico



COD: 66262

En la prestación de los primeros auxilios, la evaluación inicial del herido tiene por objetivo:

A determinar el alcance de las lesiones.

B conseguir la presencia de una ambulancia.

C conseguir información sobre la forma de contactar con sus allegados.

D Todas las respuestas son correctas.

RESPUESTA: A

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66263

En la prestación de los primeros auxilios, la valoración primaria del herido tiene como finalidad:

A detectar todas sus heridas externas.

B identificar situaciones que supongan una amenaza inmediata para su vida.

C detectar las heridas externas más evidentes.

D Las respuestas B y C son correctas.

RESPUESTA: B

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66264

¿En qué aspecto se centra la valoración primaria del herido al prestar los primeros auxilios?

A Respiración y movilidad de columna.

B Consciencia, respiración y movilidad de la columna.

C Consciencia, respiración y circulación.

D Nivel de consciencia, únicamente.

RESPUESTA: C

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66265

En la prestación de primeros auxilios, el primer paso de la valoración primaria de un herido es:

A determinar si respira.

B valorar el nivel de consciencia.

C comprobar si tiene heridas graves.

D comprobar si tiene pulso.

RESPUESTA: B

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66266

¿Qué técnica se recomienda utilizar para valorar el nivel de consciencia en un herido en accidente de tráfico?

A pellizcarle en el dorso de la mano.

B sacudirlo por los hombros.

C sacudirlo por las piernas.

D Todas las respuestas son correctas.

RESPUESTA: A

NORMA: Manual de primeros auxilios para profesores de formación vial. Dirección General de Tráfico

REFERENCIA DOCTRINAL: Manual de primeros auxilios para profesores de formación vial. Dirección General de Tráfico



COD: 66267

En la prestación de primeros auxilios, una vez hayamos determinado que el herido está consciente:

A hay que pasar siempre a valorar si respira.

B hay que pasar a valorar si respira cuando el nivel de consciencia sea bajo.

C hay que pasar a valorar si respira cuando tenga heridas en los pulmones.

D no es necesario pasar a valorar si respira.

RESPUESTA: D

NORMA: Manual de primeros auxilios para profesores de formación vial. Dirección General de Tráfico

REFERENCIA DOCTRINAL: Manual de primeros auxilios para profesores de formación vial. Dirección General de Tráfico



COD: 66268

¿En qué posición no se produce la obstrucción de la vía aérea de un herido inconsciente por caída de la lengua hacia atrás?

A Tendido boca arriba.

B Tendido boca abajo.

C Tendido de lado.

D Las respuestas B y C son correctas.

RESPUESTA: D

NORMA: Manual de primeros auxilios para profesores de formación vial. Dirección General de Tráfico

REFERENCIA DOCTRINAL: Manual de primeros auxilios para profesores de formación vial. Dirección General de Tráfico



COD: 66269

¿Cómo se ejecuta la maniobra frente-mentón en un herido inconsciente?

A Empujando el mentón hacia abajo y la frente hacia delante.

B Empujando el mentón hacia arriba y la frente hacia atrás.

C Empujando la frente y el mentón hacia abajo.

D Esta maniobra no se debe practicar en heridos que no estén conscientes, dado el riesgo de causarles lesiones de médula.

RESPUESTA: B

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66270

¿Qué es lo primero que se debe hacer con un herido inconsciente tras aplicarle la maniobra frente-mentón?

A Colocarlo en posición de defensa.

B Colocarlo boca abajo y con los brazos  por encima de la cabeza doblados en ángulo de 90 grados.

C Comprobar si tiene objetos extraños en la boca y extraerlos, si es el caso y resulta posible.

D Practicarle el boca a boca.

RESPUESTA: C

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66271

En la prestación de los primeros auxilios, ¿cómo se comprueba si un herido inconsciente respira?

A Acercando el oído a su boca y su nariz.

B Observando si se mueve el tórax y el abdomen.

C Intentando sentir en la mejilla el aire expirado.

D Todas las respuestas son correctas.

RESPUESTA: D

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66272

Si un herido en accidente de tráfico sufre una hemorragia grave, como primera medida:

A hay que hacer compresión sobre el punto de sangrado, sin levantar el apósito en ningún caso.

B hay que hacer compresión sobre el punto de sangrado, cambiando el apósito cuando se empape de sangre.

C hay que ponerlo en posición lateral de seguridad.

D hay que hacerle un torniquete.

RESPUESTA: A

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66273

Para colocar a un herido inconsciente en posición lateral de seguridad:

A el brazo más próximo a nosotros se extiende hacia arriba y la pierna contraria se flexiona.

B el brazo más alejado de nosotros se extiende hacia arriba y la pierna contraria se flexiona.

C los dos brazos se extienden hacia arriba y la pierna más alejada de nosotros se flexiona.

D el brazo más próximo a nosotros se extiende hacia arriba y las dos piernas se flexionan.

RESPUESTA: A

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66274

En la prestación de los primeros auxilios, la valoración secundaria de un herido:

A consiste en aplicar las técnicas necesarias para conservar las constantes vitales.

B consiste en una exploración detallada para determinar el alcance de las lesiones.

C consiste en una exploración rápida para identificar las heridas más graves.

D es la que realiza el equipo médico que acude a la llamada de socorro, tras haberse aplicado los primeros auxilios por los testigos del accidente.

RESPUESTA: B

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66275

En la prestación de los primeros auxilios, ¿qué comprobaciones incluye la valoración secundaria de un herido?

A coloración de la piel.

B temperatura.

C estado de la cabeza.

D Todas las respuestas son correctas.

RESPUESTA: D

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66276

¿Qué aspectos hay que revisar para hacer el examen neurológico de un herido al prestarle primeros auxilios?

A Nivel de consciencia.

B Orientación espacial.

C Pupilas.

D Todas las respuestas son correctas.

RESPUESTA: D

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66277

¿Cuál es la frecuencia respiratoria normal?

A De 60 a 100 veces por minuto.

B De 40 a 60 veces por minuto.

C De 12 a 20 veces por minuto.

D De 10 a 30 veces por minuto.

RESPUESTA: C

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66278

En la prestación de los primeros auxilios, la técnica del relleno capilar se utiliza para:

A insuflar aire en los pulmones.

B taponar las heridas que sangran mucho.

C conocer la capacidad del sistema circulatorio para restablecer el riego en una zona.

D taponar las heridas que no sangran en exceso.

RESPUESTA: C

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66279

En la prestación de los primeros auxilios, ¿cómo se llama la técnica que permite conocer la capacidad del sistema circulatorio para restaurar el riego en una zona?

A Boca a boca.

B Relleno capilar.

C Transfusión.

D Resucitación cardiopulmonar.

RESPUESTA: B

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66283

En las hemorragias venosas:

A la sangre es menos oscura que en las arteriales.

B la sangre fluye coincidiendo con los latidos del corazón.

C la sangre fluye de modo continuo.

D Las respuestas A y C son correctas.

RESPUESTA: C

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66284

Si la presión sobre el punto de sangrado no es suficiente para cortar una hemorragia externa:

A se elevará la zona afectada sin dejar de presionar sobre la herida.

B se elevará la zona afectada dejando de hacer presión sobre la herida.

C se procurará situar la zona afectada a la misma altura que el corazón para que la presión se estabilice.

D se situará la zona afectada lo más bajo posible, aumentando la presión sobre la herida.

RESPUESTA: A

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66285

En caso de hemorragia en una pierna:

A se presionará sobre la arteria humeral.

B se presionará sobre la arteria femoral.

C se presionará sobre la arteria carótida.

D no se debe presionar nunca en la arteria, pues saldrá más sangre.

RESPUESTA: B

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66286

¿Qué material es más adecuado para efectuar un torniquete?

A Una cuerda fina.

B Un cable.

C Una goma elástica.

D Una cuerda gruesa.

RESPUESTA: C

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66287

Una vez efectuado un torniquete:

A hay que tapar la zona si hace frío.

B hay que tapar la zona si hace mucho sol para que la radiación solar no impacte sobre la herida.

C hay que tapar la zona si lo pide el herido.

D no se debe tapar nunca para que no pase desapercibido.

RESPUESTA: D

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66288

El shock hipovolémico se produce por:

A reducción del volumen de sangre en el sistema circulatorio.

B falta de riego de órganos vitales.

C afluencia excesiva de sangre al cerebro.

D Las respuestas A y B son correctas.

RESPUESTA: D

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66289

¿Cómo hay que colocar a un herido que esté sufriendo un shock hipovolémico?

A Con las piernas levantadas 45 grados respecto de la cabeza.

B Con la cabeza elevada 45 grados respecto de las piernas.

C Con la cabeza elevada 45 grados respecto de las piernas y los brazos levantados a su vez por encima de la cabeza.

D Tumbado, con la cabeza a la misma altura que las piernas.

RESPUESTA: A

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66290

¿Qué es una fractura de hueso abierta?

A Aquella en la que los tendones del hueso fracturado también se han roto.

B Aquella en la que la piel que recubre el hueso fracturado también presenta daño.

C Aquella que presenta los fragmentos del hueso muy separados entre sí.

D Aquella que todavía no ha sido reducida.

RESPUESTA: B

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66291

¿Qué se puede hacer para auxiliar a una persona que haya sufrido la fractura de un hueso?

A Sujetarle el hueso fracturado con nuestras manos.

B Ponerle un cabestrillo si se trata de un brazo.

C Ponerle una férula si se trata de una pierna.

D Todas las respuestas son correctas.

RESPUESTA: D

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66295

Son signos de infección de una herida:

A bajada de la tensión arterial.

B hipotermia.

C calor en la zona de la herida.

D aceleración del pulso.

RESPUESTA: C

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66296

En primeros auxilios, el tratamiento de una herida superficial conlleva:

A limpiar la herida con agua.

B retirar cuerpos extraños que no ofrezcan dificultad.

C dejarla al descubierto si no expulsa líquidos.

D Todas las respuestas son correctas.

RESPUESTA: D

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66298

Las probabilidades de supervivencia en caso de parada cardiorrespiratoria disminuyen con cada minuto sin aporte de oxígeno al cerebro:

A un 5 %.

B un 20 %.

C un 30 %.

D un 10 %.

RESPUESTA: D

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66299

En la reanimación cardiopulmonar, las compresiones torácicas se aplican a un ritmo de:

A 50 veces por minuto.

B 100 veces por minuto.

C 30 veces por minuto.

D 80 veces por minuto.

RESPUESTA: B

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66300

En la reanimación cardiopulmonar, las compresiones torácicas se aplican con los brazos:

A perpendiculares al pecho.

B inclinados respecto del pecho, empujando hacia la zona de la cabeza.

C inclinados respecto del pecho, empujando hacia la zona derecha del cuerpo.

D inclinados respecto del pecho, empujando hacia la zona izquierda del cuerpo.

RESPUESTA: A

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



COD: 66301

¿Qué operaciones se ejecutan durante la reanimación cardiopulmonar para efectuar las insuflaciones boca a boca?

A Aplicar la maniobra frente-mentón.

B Pinzar la nariz.

C Comprobar que el pecho se eleva tras insuflar el aire.

D Todas las respuestas son correctas.

RESPUESTA: D

NORMA: Manual básico de primeros auxilios. Cruz Roja Española

REFERENCIA DOCTRINAL: Manual básico de primeros auxilios. Cruz Roja Española



